# Bayesian multivariate linear model for UK chlorophyll-fluorometer relationship

```{r setup, include=FALSE}
require(rstan, quietly=T)
require(cefasMOS)
rstan_options(auto_write = TRUE)
options(mc.cores = parallel::detectCores())
Sys.setenv(tz="UTC")
```

# Overview

## Seasonality

Season is an variable, but is "circular" as in month 12 is adjacent to both 11 and 1.
the Von Mises distibution is the circular variant of the normal distibution, with a "mean" (location, $\mu$) parameter and a "standard deviation" (precision, $\kappa$).
If $\kappa$ is 0 the distibution is uniform, for small $\kappa$ is it close to uniform.

```{r von_mises}
  # examples of von-mises using the brms package

mu = 2 # the location
kappa = 1 # the precision
dist = brms::rvon_mises(1000, mu, kappa)
hist(dist)
```

As shown the distribution loops around between +$\pi$ and -$\pi$.
In order for the density to properly normalise the $\mu$ parameter needs to be restricted to some interval of length 2$\pi$.
A solar (tropical) year is 365 ephemeris days (86400 seconds), 5 hours, 48 minutes and 45 seconds.

```{r von_mises_scaled}
solar_year = (365 * 86400) + (60*60*5) + (48*60) + 45
scale_factor = 365/(2*pi) # leap years?
# scale_factor = solar_year / (2*pi)
hist(brms::rvon_mises(1000, 182/scale_factor, 1*scale_factor), breaks=100, xlim=c(-pi, +pi))
```

## Region

We could divide the UK shelf seas into "arbitary" regions our model,
perhaps draw a line and say this data is in the Celtic Sea and this other data the Irish.
If our phytoplankton communities don't respect these human boundaries our model will be forced to make some bad assumtions.
A better method is to use the spatial coordinates, thus giving the model the flexablity to explore the whole spatial parameter space rather than discrete boxes.

There are two good ways we can think of:
1. using a covariance matrix and points represening the centre of a region *Thanks Carolyn*.
This will need to be tweaked to avoid covariance between regions blocked by land.
The Irish Sea region covarying with the Western North Sea for instance.
2. tracing a loop around the UK with distance along the loop being a variable.

These probably will behave similarly, with the covariance matrix probably being the most simple to implement.

## Distance from land

```{r}
map_ = rworldmap::getMap("low")

x = subset(map_, (LON %between% c(-50, 25)) & (LAT %between% c(45, 70)))

mapdata = data.table(fortify(rworldmap::getMap("low")))
mapdata = mapdata[mapdata[,.I[any(long %between% c(-50, 25)) & any(lat %between% c(45, 70))], by = list(group)]$V1]

ggplot() +
  geom_polygon(data=mapdata, aes(long, lat, group=group))
```

## Depth

## Height

## Temperature

## Salinity

# The data

For this run the sources of data are:
1. chlorophyll data held within LIMS,
Kate uploaded HPLC data from the HIGHROC project with "HPLC" in the comment field.
1. Seapoint fluorometer data from ferrybox archive
1. Seapoint fluorometer data from SmartBuoy from the SmartBuoy database
1. Seapoint fluorometer data from ESM2 profiler, also from the SmartBuoy database

```{r ferrybox, eval=F}
  # ferrybox
  # get names for ferrybox .rdata files
fb_files = list.files("../FerryboxQC/",
                      pattern="fb20.+rdata", full.names=T, recursive=T)

  # load each file
for(file in fb_files){ load(file) }
  # get names of loaded data
fb_files = ls(pattern="fb20*")

tids = c(1190, 1192, 144) # variables of interest, temp, sal, fluors

  # for each file subset and add to list
fb = list()
for(file in fb_files){
  fb[[file]] = get(file)[telid %in% tids,.(dateTime, Latitude, Longitude, variable, Quality, Mean, serial, Variance, Count)]
}
  # turn the list into one big data.table
fb = rbindlist(fb)

  # select only good data and remove duplicates
fb = unique(fb[Quality == 0])
  # convert to numeric
fb[, Mean := as.numeric(Mean)]
fb[, Variance := as.numeric(Variance)]
fb[, Count := as.numeric(Count)]

  # reshape to wide
fb = dcast.data.table(fb, dateTime + Latitude + Longitude ~ variable, value.var=c("Mean", "serial"))

save(fb, file="ferrybox_flu.rdata")
```

```{r smartbuoy, eval=F}
  # get the list of smartbuoys
deps = smartbuoy.positions()[platform == "SmartBuoy"]$deployment
  # fetch the data from the database
sb = smartbuoy.fetch(parameters=c("FLUORS", "SAL", "TEMP"), deployment_group=deps)

sb = unique(sb)
  # reshape the data, where duplicates, take the mean
sb = dcast.data.table(sb, dateTime + lat + lon + deployment_group ~ par,
                      value.var=c("value", "sensor_serial"), fun.aggregate=first, fill=NA)

  # discard where there is no flu
sb = sb[!is.na(value_FLUORS)]
save(sb, file="smartbuoy_flu.rdata")
```

```{r profiler, eval=F}
  # fetch all profiler data
pr = profiler.fetch(parameters=c("TEMP", "SAL", "FLUORS"))
  # aggregate the data into 1 meter bins using just the up cast (the cast where bottles are fired)
save(pr, file="pr_raw.rdata")
pr = profiler.binning(pr, bin_height=1, use_cast="UP")
  # reshape
pr = dcast.data.table(pr, startTime + endTime + latitude + longitude + depth_bin ~ par,
                      value.var=c("bin_mean", "serial"), fill=NA, fun.aggregate=first)
save(pr, file="pr_flu.rdata")
```

```{r lims, eval=F}
chl = lims.fetch(parameters="chlorophyll")

save(chl, file="lims_chl.rdata")

ggplot(chl) +
  geom_histogram(aes(value)) +
  labs(x=expression("Chlorophyll"~(mu*g/l))) +
  theme_bw()

```

```{r combine, eval=F}
load("lims_chl.rdata")
load("ferrybox_flu.rdata")
load("pr_flu.rdata")
load("smartbuoy_flu.rdata")

  # rename columns
fb = fb[,.(dateTime, lat = Latitude, lon = Longitude,
            FLU = Mean_FLUORS, SAL = Mean_SAL, TEMP = Mean_TEMP,
            depth = 4, serial = serial_FLUORS, source = "ferrybox")]

pr = pr[,.(dateTime = startTime, lat = latitude, lon = longitude,
            FLU = bin_mean_FLUORS, SAL = bin_mean_SAL, TEMP = bin_mean_TEMP,
            depth = depth_bin, serial = serial_FLUORS, source = "profiler")]

sb = sb[,.(dateTime, lat, lon,
        FLU = value_FLUORS, SAL = value_SAL, TEMP = value_TEMP,
        depth = 1, serial = sensor_serial_FLUORS, source="smartbuoy")]

  # subset by time
  # fb is every minute so no need to round
fb = fb[dateTime %in% chl$dateTime]
sb = sb[dateTime %in% round_minute(chl$dateTime, 10)]
pr = pr[round_minute(dateTime, 10) %in% round_minute(chl$dateTime, 10)]

  # match to closests times
fb = fuzzymatch(fb, chl[depth < 6], threshold=60*3)
sb = fuzzymatch(sb, chl[depth < 6], threshold=60*15)
pr = fuzzymatch(pr, chl, threshold=60*6)
  # be fuzzy with depth matching
pr = pr[depth %between% c(i.depth+1, i.depth-1)]
  # average depths
pr[, ]

  # stick all the data together
d = rbind(fb, sb, pr)
  # get rid of non UK stuff
d = d[lon %between% c(-20, 20) & lat %between% c(40, 60)]
d = d[longitude %between% c(-20, 20) & latitude %between% c(40, 60)]
  # calculate distance between chl and source data
d[, dist := geosphere::distGeo(data.frame(lon, lat), data.frame(longitude, latitude))]
  # wthin 5k reasonable?
d = d[dist < 5000]
  # reorder
d = d[order(dateTime)]

ggplot(d) + geom_point(aes(FLU, value, color=serial))

save(d, file="combined.rdata")

ggplot(d) + geom_point(aes(lon, lat))

```

# The model

## M1 - Flu Vs Chl

Lets first start with the most basic linear model with very uninformative priors.

$$C_i \sim Normal(\mu, \sigma) \tag{likelihood}$$

$$\mu_i + \alpha + \beta F_i \tag{linear model}$$

$$\alpha \sim Normal(0, 10) \tag{$\alpha$ prior}$$

$$\beta \sim Normal(0, 10) \tag{$\beta$ prior}$$

$$\sigma \sim Uniform(0, 50) \tag{$\sigma$ prior}$$

Where $C$ is the measured chlorophyll and $F$ is the fluorometry.

```{r m1}
require(rethinking)

m1 = map(
  alist(
    value ~ dnorm(mu, sigma),
    mu <- a + b * FLU,
    a ~ dnorm(0, 10),
    b ~ dnorm(0, 10),
    sigma ~ dunif(0, 50)
  ),
  data = as.data.frame(d)
)
print(m1)
fit = lm(data=d, value ~ FLU)
print(fit)
sd(fit$residuals)
```

`m1` is equivilent to the assumtions of a `lm` linear model and as you can see the MAP values from `m1` are basically equivilent to the `lm` coefficents.
